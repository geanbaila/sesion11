**Este es nuestro primer proyecto usando git**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Dependencias

Usted necesita instalar Struts2 

- http://plugins.netbeans.org/plugin/39218

## Imagen de la web


![alt text](https://bitbucket.org/geanbaila/sesion11/raw/f52bf51874db44adf37161b4769cfd7b243ad79c/web/assets/img/web.PNG)
