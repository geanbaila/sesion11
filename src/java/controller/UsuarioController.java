/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.util.logging.Logger;
import model.UsuarioModel;

/**
 *
 * @author Administrador
 */

public class UsuarioController extends ActionSupport{
 
    private String nombres;
    private String email;

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String registrar() {  
        UsuarioModel usuarioModel = new UsuarioModel();
        usuarioModel.regirtrar(getNombres(),getEmail());
        
        return SUCCESS; 
    }
    
}
