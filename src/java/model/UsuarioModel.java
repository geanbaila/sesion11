/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrador
 */
public class UsuarioModel extends Database{    
    public void regirtrar(String nombres, String email) {   
        try {
            this.connect();
            if (this.getConn() != null) {
                String sql = "insert into usuarios (nombres, email) "
                        +"values(?,?)";
                System.out.println(sql);
                PreparedStatement preparedStmt = this.getConn()
                        .prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                //para los campos
                preparedStmt.setString(1, nombres);
                preparedStmt.setString(2, email);
                int affects = preparedStmt.executeUpdate();
                this.disconnect();
            }
        } catch (ClassNotFoundException ex) {
            System.out.println("Ha ocurrido un error 1: "+ex.getCause());
        } catch (SQLException ex) {
            System.out.println("Ha ocurrido un error 2: "+ex.getCause());
        }
    }
}
